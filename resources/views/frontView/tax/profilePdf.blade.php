<html>
<head>
    <style>
        table {
          border-collapse: collapse;
          width: 100%;
        }
        
        th, td {
          padding: 8px;
          text-align: left;
          border-bottom: 1px solid #ddd;
        }
      
      #chk {
        width: 30px;
        height: 30px;
        border: 1px solid green;
      }
        </style>
</head>
<body>
  <table>
    <tr>
      <th width="50%">
        <h3>Name: {{$client->fname.' '.$client->lname }}</h3>
        <p>US Information Request</p> 
      </th>
      <th style="text-align: right" width="50%">
<!--         <img height="80px" src="{{asset('img/tax_logo_black.jpg')}}">   -->
      </th>

    </tr>

  </table>
    <div class="">
        <div class="row mt-5">
            <div class="col-md-9 col-sm-9">
                <h4><u>General Request</u></h4>
                
                <table style="width: 100%" class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Comment</th>
                        <th>Provided</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tbody>
                            <tr>
                                <td>1. Tax Organizar</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><div style="border-style: solid;border-color: black;float: right;border-width:1px; width: 10px; height: 10px;"></div></td>
                            </tr>
                            <tr>
                                <td>2. FBAR Organizar</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><div style="border-style: solid;border-color: black;float: right;border-width:1px; width: 10px; height: 10px;"></div></td>
                            </tr>
                            <tr>
                                <td>3. IRS Notice</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><div style="border-style: solid;border-color: black;float: right;border-width:1px; width: 10px; height: 10px;"></div></td>
                            </tr>
                            <tr>
                                <td>4. Last us tax return file</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><div style="border-style: solid;border-color: black;float: right;border-width:1px; width: 10px; height: 10px;"></div></td>
                            </tr>
                        </tbody>
                    </tbody>
                </table>
            </div>
            <div class="col-md-3 col-sm-3">
                    <div class="vl"></div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <h4><u>Income Type</u></h4>
                <table style="width: 100%" class="table">
                    <thead>
                      <tr>
                        <th>Income Type</th>
                        <th>Country</th>
                        <th>Form Required</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tbody>
                            @foreach ($txtProfileDocList as $tp)
                                <tr>
                                    <td><?php if($tp->docx != null) echo $tp->docx->name; ?></td>
                                    <td><?php if($tp->country_name != null) echo $tp->country_name->country_name ?></td>
                                    <td><?= $tp->dox ?></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </tbody>
              
                </table>
          <p style="margin-top:30px">
          <?php if(isset($client->notes) && ($client->notes != '') ){ ?>
            <b>Notes:</b><br>
            <?= $client->notes ?>
          <?php } ?>
          </p>
          
            </div>
        </div>
      </div>
</body>
</html>