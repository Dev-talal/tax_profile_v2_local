<?php $__env->startSection('main_body'); ?>

<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Wizard 6-->
                <div class="wizard wizard-6 d-flex flex-column flex-column-fluid" id="kt_wizard">
                    <!--begin::Container-->
                    <div class="wizard-content d-flex flex-column ml-10 mr-10 py-10 py-lg-20">
                        <!--begin::Nav-->
                        <div class="d-flex flex-column-auto flex-column px-10">
                            <!--begin: Wizard Nav-->
                            <div class="wizard-nav pb-lg-10 pb-3 d-flex flex-column align-items-center align-items-md-start">
                                <!--begin::Wizard Steps-->
                                <div class="wizard-steps d-flex flex-column flex-md-row">


                                    
                                    <!--end::Wizard Step 3 Nav-->
                                </div>
                                <!--end::Wizard Steps-->
                            </div>
                            <!--end: Wizard Nav-->
                        </div>
                        <!--end::Nav-->

                        <!--begin::Form-->
                        <form class="" method="post" action="saveTaxProfile" novalidate="novalidate" id="kt_wizard_form">
                            <?php echo e(csrf_field()); ?>

                                                        

                            <!--begin: Wizard Step 3-->
                            <div class="pb-5" data-wizard-type="step-content">
                                <div style="text-align: center;" class="row">
                                    <div style="height: 220px" style="text-align: center; border-color: #000;" class="col-md-5 mt-10 col-sm-12 card">
                                            <h3 class=" mt-1">Client Personal details</h3>
                                            
                                            <div class="row offset-3">
                                                <img height="100px" src="http://127.0.0.1:8000/img/person.png">
                                                <div style="text-align: left" class="mt-4 ml-2">
                                                    <ul>
                                                    <li><label id="m_name">Name: <?php echo e($client->fname.' '.$client->lname); ?></label></li>
                                                        <li><label id="m_dob">Born: <?php echo e($client->dob); ?></label></li>
                                                        <li><label id="m_relation">City: <?php echo e($client->city); ?></label></li>
                                                    </ul>
                                                </div>
                                            </div>
                                    </div>
                                    <div style="height: 220px" style="text-align: center; border-color: #000;" class="offset-md-2 mt-10 col-md-5 col-sm-12 card">
                                        <div>
                                        <h3 class="mt-1">Client Family Details</h3>
                                        
                                        </div>
                                        <div class="row offset-3">
                                            <img height="100px" src="http://127.0.0.1:8000/img/family.png">
                                            <div style="text-align: left" class="mt-4 ml-2">
                                                <ul>
                                                    <li><label id="r_name">Name: <?php echo e($client->sfname.' '.$client->slname); ?></label></li>
                                                    <li><label id="r_dob">Born: <?php echo e($client->sdob); ?></label></li>
                                                </ul>
                                            </div>
                                        </div>
                                </div>
                                </div>
                                <hr class="mt-10">
                                <div class="row mt-10">
                                    <div class="mt-10 col-md-12 col-sm-12">
                                        <h2 class="mt-10 font-weight-bold text-dark">List of documents required by client</h2>
                                        <span class="form-text text-muted">As the client has choosen the following income types so he needs to submit the following documents.</span>
                                        
                                    </div>
                                    <div class="col-md-12 example-preview">
                                        <table id="tblIncome" class="table mb-5 table-striped">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">Income type</th>
                                                    <th scope="col">Country</th>
                                                    <th scope="col">Form Required</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $txtProfileDocList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?= $tp->docx->name ?></td>
                                                        <td><?= $tp->country_name->country_name ?></td>
                                                        <td><?= $tp->dox ?></td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                        <a href="/profilePdf/<?= $id ?>" style="float: right" class="btn btn-sm btn-secondary">Download Pdf</a>
                                    </div>  
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Wizard 6-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>
    <!--end::Container-->
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontView.masterView', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>