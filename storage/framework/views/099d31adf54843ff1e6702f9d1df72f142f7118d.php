<?php $__env->startSection('main_body'); ?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Wizard-->
                <div class="wizard wizard-1" id="kt_wizard_v1" data-wizard-state="first" data-wizard-clickable="false">
                    <!--begin::Wizard Nav-->
                    <!--end::Wizard Nav-->
                    <!--begin::Wizard Body-->
                    <div class="row my-10 px-8 my-lg-15 px-lg-10">
                        <div class="col-xl-12 col-xxl-12">
                            <!--begin::Wizard Form-->
                            <form method="post" class="form fv-plugins-bootstrap fv-plugins-framework" id="kt_form" action="addCountry">
                                <!--begin::Wizard Step 1-->
                                <?php echo e(csrf_field()); ?>

                                <div class="row">
                                    <div class="col-md-12 offset-3 col-sm-12">
                                        <h3 class="font-weight-bold text-dark">Income Type</h3>
                                            <div class="mb-10 fv-plugins-message-container">Add new Income Type here to display on Tax Profile</div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div  style="margin-top: 20%; margin-left:30%">
                                            <img src="https://preview.keenthemes.com/metronic/theme/html/demo1/dist/assets/media/users/300_21.jpg" width="200px" class="rounded-circle"><br>
                                            <a href="#">Upload Picture/icon here</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div>
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Incom Type Category</label>
                                                <select  class="form-control form-control-solid form-control-lg">
                                                    <option>Select Incom Category</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div>
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Incom Type Name</label>
                                                <input type="text" class="form-control form-control-solid form-control-lg" name="name" id="name" placeholder="Country Name">
                                            </div>
                                        </div>
                                        <div>
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Form Type</label>
                                                <select  class="form-control form-control-solid form-control-lg">
                                                    <option>Select Form type</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div>
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Minimum Fee</label>
                                                <input type="number" class="form-control form-control-solid form-control-lg" name="name" id="name" placeholder="e.g 10">
                                            </div>
                                        </div>
                                        <div>
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Maximum Fee</label>
                                                <input type="number" class="form-control form-control-solid form-control-lg" name="name" id="name" placeholder="e.g 120">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <h3 class="font-weight-bold text-dark">Documnet required by country:</h3>
                                            <div class="mb-10 fv-plugins-message-container">Write down the documents required by each country, in the relevant country field</div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>Country</label>
                                            <select  class="form-control form-control-solid form-control-lg">
                                                <option>Select Country</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div id="doc_div">
                                            <div>
                                                <!--begin::Input-->
                                                <div class="form-group fv-plugins-icon-container">
                                                    <label>Document Needed</label>
                                                    <input type="text" class="form-control form-control-solid form-control-lg" name="ducuments[]" id="name" placeholder="E.g statement of investement income">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <a style="cursor:pointer" onclick="addmore()" class="float-right" ><u>Add More</u></a>
                                    </div>
                                </div>
                                
                                <div class="d-flex justify-content-between mt-5 pt-10">
                                    <div class="mr-2">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-next">add</button>
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                    <!--end::Wizard Body-->
                </div>

         <script>
         function addmore(){
             $('#doc_div').append('<div><div class="form-group fv-plugins-icon-container"><label>Document Needed</label><input type="text" class="form-control form-control-solid form-control-lg" name="ducuments[]" placeholder="E.g statement of investement income"></div></div>');
         }
         </script>

                <!--end::Wizard-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>




</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontView.masterView', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>