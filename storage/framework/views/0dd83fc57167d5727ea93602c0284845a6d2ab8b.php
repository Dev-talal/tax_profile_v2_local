<?php $__env->startSection('main_body'); ?>

<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container" style="margin:30px 0px;">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Wizard 6-->
                <div class="wizard wizard-6 d-flex flex-column flex-column-fluid" id="kt_wizard">
                    <!--begin::Container-->
                    <div class="wizard-content d-flex flex-column ml-10 mr-10">
                        <!--begin::Nav-->
                        <div class="d-flex flex-column-auto flex-column px-10">
                            <!--begin: Wizard Nav-->
                            <div class="wizard-nav pb-lg-10 pb-3 d-flex flex-column align-items-center align-items-md-start">
                                <!--begin::Wizard Steps-->
                                <div class="wizard-steps d-flex flex-column flex-md-row">


                                    
                                    <!--end::Wizard Step 3 Nav-->
                                </div>
                                <!--end::Wizard Steps-->
                            </div>
                            <!--end: Wizard Nav-->
                        </div>
                        <!--end::Nav-->

                        <!--begin::Form-->
                        <form class="" method="post" action="saveTaxProfile" novalidate="novalidate" id="kt_wizard_form">
                            <?php echo e(csrf_field()); ?>

                                                        

                            <!--begin: Wizard Step 3-->
                            <div class="pb-5" data-wizard-type="step-content">
                                <h2 class="mt-md-5 mt-2">Client View</h2>
                                <div style="text-align: center;" class="row">
                                    <div style="height: 220px" style="text-align: center; border-color: #000;" class="col-md-5 mt-10 col-sm-12 card">
                                            <h3 class="mt-md-5 mt-2">Client Personal details</h3>
                                            
                                            <div class="row mt-md-5 mt-2">
                                                <div class="col-md-4">
                                                    <img height="100px" src="<?php echo e(asset('img/person.PNG')); ?>">
                                                </div>
                                                <div class="col-md-8">
                                                    <div style="text-align: left" class="mt-4 ml-2">
                                                    <ul>
                                                    <li><label id="m_name">Name: <?php echo e($client->fname.' '.$client->lname); ?></label></li>
                                                        <li><label id="m_dob">Born: <?php echo e($client->dob); ?></label></li>
                                                        <li><label id="m_relation">City: <?php echo e($client->city); ?></label></li>
                                                    </ul>
                                                </div>
                                                </div>
                                                
                                            </div>
                                    </div>
                                  <?php $temp= '';
                                        if($client->msSingle == 1){
                                                 $temp = 'hidden';
                                                    }
                                  ?>
                                    <div <?php echo e($temp); ?> style="height: 220px" style="text-align: center; border-color: #000;" class="offset-md-2 mt-10 col-md-5 col-sm-12 card">
                                        <div>
                                        <h3 class="mt-md-5 mt-2">Client Family Details</h3>
                                        
                                        </div>
                                        <div class="row mt-md-5 mt-2">
                                            <div class="col-md-4">
                                                <img height="100px" src="<?php echo e(asset('img/family.PNG')); ?>">
                                            </div>
                                            <div class="col-md-8">
                                                <div style="text-align: left" class="mt-4 ml-2">
                                                <ul>
                                                    <li><label id="r_name">Name: <?php echo e($client->sfname.' '.$client->slname); ?></label></li>
                                                    <li><label id="r_dob">Born: <?php echo e($client->sdob); ?></label></li>
                                                </ul>
                                            </div>
                                            </div>
                                        </div>
                                </div>
                                </div>
                                <hr class="mt-10">
                                <div class="row mt-10">
                                    <div class="mt-10 col-md-12 col-sm-12">
                                        <h2 class="mt-10 font-weight-bold text-dark">List of documents required by client</h2>
                                        <span class="form-text text-muted mb-md-5 mb-3">As the client has choosen the following income types so he needs to submit the following documents.</span>
                                        
                                    </div>
                                    <div class="col-md-12 example-preview">
                                        <table id="tblIncome" class="table mb-5 table-striped">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">Income type</th>
                                                    <th scope="col">Country</th>
                                                    <th scope="col">Form Required</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $__currentLoopData = $txtProfileDocList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php if($tp->docx != null) echo $tp->docx->name; ?></td>
                                                        <td><?php if($tp->country_name != null) echo $tp->country_name->country_name ?></td>
                                                        <td><?= $tp->dox ?></td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                      
                                      <p>
                                        <?php if(isset($client->notes) && ($client->notes != '') ){ ?>
                                          <b>Notes:</b><br>
                                          <?= $client->notes ?>
                                        <?php } ?>
                                      </p>
                                        <a onclick="NewTab('/profilePdf/<?= $id ?>')" style="float: right" class="btn btn-sm btn-secondary">Download Pdf</a>
                                    </div>  
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                  <script>
                    function NewTab(url) { 
                        window.open(url, "_blank"); 
                    } 
                  </script>
                    <!--end::Container-->
                </div>
                <!--end::Wizard 6-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>
    <!--end::Container-->
</div>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontView.masterView', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>