<?php $__env->startSection('main_body'); ?>

<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Wizard 6-->
                <div class="wizard wizard-6 d-flex flex-column flex-lg-row flex-column-fluid" id="kt_wizard">
                    <!--begin::Container-->
                    <div class="wizard-content d-flex flex-column mx-auto py-10 py-lg-20 w-100 w-md-700px">
                        <!--begin::Nav-->
                        <div class="d-flex flex-column-auto flex-column px-10">
                            <!--begin: Wizard Nav-->
                            <div class="wizard-nav pb-lg-10 pb-3 d-flex flex-column align-items-center align-items-md-start">
                                <!--begin::Wizard Steps-->
                                <div class="wizard-steps d-flex flex-column flex-md-row">
                                    <!--begin::Wizard Step 1 Nav-->
                                    <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step" data-wizard-state="current">
                                        <div class="wizard-wrapper pr-lg-7 pr-5">
                                            <div class="wizard-icon">
                                                <i class="wizard-check ki ki-check"></i>
                                                <span class="wizard-number">1</span>
                                            </div>
                                            <div class="wizard-label mr-3">
                                                <h3 class="wizard-title">Account</h3>
                                                <div class="wizard-desc">Account details</div>
                                            </div>
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24" />
                                                        <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
                                                        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 1 Nav-->
                                    <!--begin::Wizard Step 2 Nav-->
                                    <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                                        <div class="wizard-wrapper pr-lg-7 pr-5">
                                            <div class="wizard-icon">
                                                <i class="wizard-check ki ki-check"></i>
                                                <span class="wizard-number">2</span>
                                            </div>
                                            <div class="wizard-label mr-3">
                                                <h3 class="wizard-title">Address</h3>
                                                <div class="wizard-desc">Residential address</div>
                                            </div>
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24" />
                                                        <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
                                                        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 2 Nav-->
                                    <!--begin::Wizard Step 3 Nav-->
                                    <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                                        <div class="wizard-wrapper">
                                            <div class="wizard-icon">
                                                <i class="wizard-check ki ki-check"></i>
                                                <span class="wizard-number">3</span>
                                            </div>
                                            <div class="wizard-label">
                                                <h3 class="wizard-title">Complete</h3>
                                                <div class="wizard-desc">Submit form</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 3 Nav-->
                                </div>
                                <!--end::Wizard Steps-->
                            </div>
                            <!--end: Wizard Nav-->
                        </div>
                        <!--end::Nav-->
                        <!--begin::Form-->
                        <form class="px-10" novalidate="novalidate" id="kt_wizard_form">
                            <!--begin: Wizard Step 1-->
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <h3 class="mb-10 font-weight-bold text-dark">Let's Start by entering personal details of client </h3>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>First Name</label>
                                            <input type="text" id="fname" name="fname" placeholder="First Name" required class="form-control form-control-solid form-control-lg" >
                                        </div>
                                                                                
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>Date OF Birth</label>
                                            <input type="date" id="dob" name="dob" placeholder="Date of birth" required class="form-control form-control-solid form-control-lg" >
                                            <span class="form-text text-muted">mm/dd/yyyy</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div id="doc_div">
                                            <div>
                                                <!--begin::Input-->
                                                <div class="form-group fv-plugins-icon-container">
                                                    <label>Last Name</label>
                                                    <input type="text" id="lname" name="lname" placeholder="Last Name" required class="form-control form-control-solid form-control-lg" >
                                                    <span class="form-text text-muted">Please enter the lase name from social security card.</span>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>


                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <h3 class="mb-10 font-weight-bold text-dark"></h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>Martial Status</label>
                                            <div class="row">
                                                <label class="checkbox ml-5 mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                    <input type="checkbox" id="chkSingle" name="chkSingle" >
                                                    <span></span>&nbsp;&nbsp; Single
                                                </label>
                                                <label class="checkbox ml-5 mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                    <input type="checkbox" id="chkMfj" name="chkMfj" >
                                                    <span></span>&nbsp;&nbsp; Married filling jointly
                                                </label>
                                                <label class="checkbox ml-5 mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                    <input type="checkbox" id="chkMfs" name="chkMfs" >
                                                    <span></span>&nbsp;&nbsp; Married filling Separately(MFS)
                                                </label>
                                                <label class="checkbox ml-5 mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                    <input type="checkbox" id="chkHoh" name="chkHoh" >
                                                    <span></span>&nbsp;&nbsp; Head of household(HOH)
                                                </label>
                                                <label class="checkbox ml-5 mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                    <input type="checkbox" id="chkQw" name="chkQw" >
                                                    <span></span>&nbsp;&nbsp; Qualifying widow (QW)
                                                </label>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>

                                <div class="row mt-10">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>Spouse First Name</label>
                                            <input type="text" name="sfname" id="sfname" placeholder="First Name" required class="form-control form-control-solid form-control-lg" >
                                        </div>
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>Date OF Birth</label>
                                            <input type="date" name="sdob" id="sdob" placeholder="Date of birth" required class="form-control form-control-solid form-control-lg" >
                                            <span class="form-text text-muted">mm/dd/yyyy</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div id="doc_div">
                                            <div>
                                                <!--begin::Input-->
                                                <div class="form-group fv-plugins-icon-container">
                                                    <label>Spouse Last Name</label>
                                                    <input type="text" name="slname" id="slname" placeholder="Last Name" required class="form-control form-control-solid form-control-lg" >
                                                    <span class="form-text text-muted">Please enter the lase name from social security card.</span>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>

                                <div class="row mt-10">
                                    <div class="col-md-12 col-sm-12">
                                        <h2 class="mb-10 font-weight-bold text-dark">Address</h2>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>Country</label>
                                            <select name="country" id="country" class="form-control form-control-solid form-control-lg">
                                                <option>Select Country</option>
                                            </select>
                                        </div>
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>State</label>
                                            <input type="text" name="state" id="state" placeholder="State" required class="form-control form-control-solid form-control-lg" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div id="doc_div">
                                                <div class="form-group fv-plugins-icon-container">
                                                    <label>City</label>
                                                    <input type="text" name="city" id="city" placeholder="City" required class="form-control form-control-solid form-control-lg" >
                                                </div>
                                                <div class="form-group fv-plugins-icon-container">
                                                    <label>Address</label>
                                                    <input type="text" name="address" id="address" placeholder="Address" required class="form-control form-control-solid form-control-lg" >
                                                </div>                                            
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="row mt-10">
                                    <div class="col-md-12 col-sm-12">
                                        <h2 class="mb-10 font-weight-bold text-dark">Citizenship...</h2>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div id="citizen_div">
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Citizenship</label>
                                                <select id="citizenship" name="citizenship[]" class="form-control form-control-solid form-control-lg">
                                                    <option>Select Country</option>
                                                </select>
                                            </div>
                                                                                     
                                        </div>
                                        <a style="cursor:pointer" onclick="addMoreCitizenship()" class="float-right" ><u>Add More</u></a>
                                    </div>
                                    <script>
                                        function addMoreCitizenship(){
                                            $('#citizen_div').append('<div class="form-group fv-plugins-icon-container"><select name="citizenship[]" class="form-control form-control-solid form-control-lg"><option>Select Country</option></select></div>');
                                        }
                                    </script>

                                </div>

                                <div class="row mt-10">
                                    <div class="col-md-12 col-sm-12">
                                        <h2 class="mb-10 font-weight-bold text-dark">Dependents</h2>
                                    </div>
                                        <div class="row col-md-12 col-sm-12">
                                            <div id="dependent_div" class="row col-md-12">
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>First Name</label>
                                                        <input type="text" name="dfname[]" id="dfname" placeholder="First Name" required class="form-control form-control-solid form-control-lg" >
                                                    </div>
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>Relationship to you</label>
                                                        <input type="text" name="dRelationship[]" id="dRelationship" placeholder="Relationship to you" required class="form-control form-control-solid form-control-lg" >
                                                    </div>
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                            <input type="checkbox" name="chkChildTaxCredit[]" id="chkChildTaxCredit" >
                                                            <span></span>&nbsp;&nbsp; Child tax credit
                                                        </label>
                                                    </div>
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                            <input type="checkbox" name="chkChildForOthrt[]" id="chkChildForOthrt" >
                                                            <span></span>&nbsp;&nbsp; Child for other dependents
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div id="doc_div">
                                                            <div class="form-group fv-plugins-icon-container">
                                                                <label>Last Name</label>
                                                                <input type="text" name="dlname[]" id="dlname" placeholder="Last Name" required class="form-control form-control-solid form-control-lg" >
                                                            </div>
                                                    </div>
                                                </div>                                            
                                          </div>
                                            <div class="col-md-12" >
                                                <a style="cursor:pointer" onclick="addDependent()" class="float-right" ><u>Add More</u></a>
                                            </div>
                                        </div>
                                    <script>
                                        function addDependent(){
                                            $('#dependent_div').append('<div class="col-md-6 col-sm-12">'+
                                                '<div class="form-group fv-plugins-icon-container">'+
                                                    '<label>First Name</label>'+
                                                    '<input type="text" name="dfname[]" id="dfname" placeholder="First Name" required class="form-control form-control-solid form-control-lg" >'+
                                                '</div>'+
                                                '<div class="form-group fv-plugins-icon-container">'+
                                                    '<label>Relationship to you</label>'+
                                                    '<input type="text" name="dRelationship[]" id="dRelationship" placeholder="Relationship to you" required class="form-control form-control-solid form-control-lg" >'+
                                                '</div>'+
                                                '<div class="form-group fv-plugins-icon-container">'+
                                                    '<label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">'+
                                                        '<input type="checkbox" name="chkChildTaxCredit[]" id="chkChildTaxCredit" >'+
                                                        '<span></span>&nbsp;&nbsp; Child tax credit'+
                                                    '</label>'+
                                                '</div>'+
                                                '<div class="form-group fv-plugins-icon-container">'+
                                                    '<label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">'+
                                                        '<input type="checkbox" name="chkChildForOthrt[]" id="chkChildForOthrt" >'+
                                                        '<span></span>&nbsp;&nbsp; Child for other dependents'+
                                                    '</label>'+
                                                '</div>'+
                                            '</div>'+
                                            '<div class="col-md-6 col-sm-12">'+
                                                '<div id="doc_div">'+
                                                        '<div class="form-group fv-plugins-icon-container">'+
                                                            '<label>Last Name</label>'+
                                                            '<input type="text" name="dlname[]" id="dlname" placeholder="Last Name" required class="form-control form-control-solid form-control-lg" >'+
                                                        '</div>'+
                                                '</div>'+
                                            '</div>');
                                        }
                                    </script>
                                </div>

                            </div>
                            <!--end: Wizard Step 1-->
                            <!--begin: Wizard Step 2-->
                            <div class="pb-5" data-wizard-type="step-content">

                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <h2 class="mb-10 font-weight-bold text-dark">Enter employment details, select the income type applied to the client...</h2>
                                    </div>
                                    <div class="row col-md-12">
                                    <?php $__currentLoopData = $incomeList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $income): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="row">
                                                <img height="150px" src="<?php echo e(asset('img/job.png')); ?>">
                                                <div class="mt-4 ml-2">
                                                    <?php $index = 0; ?>
                                                    <?php $__currentLoopData = $income->country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                       <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                            <input type="hidden" id="incid<?= $country->country_id ?>" value="<?= $income->id ?>">
                                                            <input type="hidden" id="incName<?= $country->country_id ?>" value="<?= $income->name ?>">
                                                            <input type="hidden" id="incDocx<?= $country->country_id ?>" value="<?=  $income->country_docx[$index] ?>">
                                                            <input type="hidden" id="countryName<?= $country->country_id ?>" value="<?= $country->country_name ?>">
                                                            <input onchange="handleChange(event)" type="checkbox" id="<?= $country->country_id ?>" name="addClient" >
                                                            <span></span>&nbsp;&nbsp; <?= $country->country_name ?>
                                                        </label>
                                                        <?php $index++; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>

                            </div>
                            <script>
                                function handleChange(e){
                                    if(e.target.checked) {
                                        var id = e.target.id;
                                        var incomeName = $('#incName'+id).val();
                                        var countryName = $('#countryName'+id).val();
                                        var docx = $('#incDocx'+id).val();
                                        $('#tblIncome').append('<tr id="tblRow'+id+'"><td>'+incomeName+'</td><td>'+countryName+'</td><td>'+docx+'</td></tr>');
                                        console.log(incomeName);
                                    } else {
                                        var id = e.target.id;
                                        $('#tblRow'+id).remove();
                                    }
                                }
                            </script>
                            <!--end: Wizard Step 2-->
                            <!--begin: Wizard Step 3-->
                            <div class="pb-5" data-wizard-type="step-content">
                                <div style="height: 250px" class="row">
                                    
                                    <div style="text-align: center; border-color: #000;" class="offset-2 col-md-5 col-sm-12 card">
                                        <div>
                                            <h3 class="mt-1">Client Family Details</h3>
                                            
                                        </div>
                                        <div class="row offset-3">
                                            <img height="100px" src="http://127.0.0.1:8000/img/family.png">
                                            <div style="text-align: left" class="mt-4 ml-2">
                                                <ul>
                                                    <li><label id="r_name">Name: Talal</label></li>
                                                    <li><label id="r_dob">Born: 12/24/1990</label></li>
                                                    <li><label id="r_relation">Childern</label></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Wizard Step 3-->
                            <!--begin: Wizard Actions-->
                            <div class="d-flex justify-content-between pt-7">
                                <div class="mr-2">
                                    <button type="button" class="btn btn-light-primary font-weight-bolder font-size-h6 pr-8 pl-6 py-4 my-3 mr-3" data-wizard-type="action-prev">
                                    <span class="svg-icon svg-icon-md mr-2">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Navigation/Left-2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1" />
                                                <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)" />
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>Previous</button>
                                </div>
                                <div>
                                    <button class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-submit" type="submit" id="kt_login_signup_form_submit_button">Submit 
                                    <span class="svg-icon svg-icon-md ml-2">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Navigation/Right-2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
                                                <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span></button>
                                    <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-next">Next 
                                    <span class="svg-icon svg-icon-md ml-2">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Navigation/Right-2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
                                                <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span></button>
                                </div>
                            </div>
                            <!--end: Wizard Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Wizard 6-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>
    <!--end::Container-->
</div>
<?php $__env->stopSection(); ?>


<script>
    function setCurrentData(){
        var fname = $('#fname').val();
        var lname = $('#lname').val();
        var dob = $('#dob').val();
        $('#m_name').html(fname+" "+ lname);
        $('#m_dob').html(dob);
        $('#m_relation').html(dob);

        var sfname = $('#sfname').val();
        var slname = $('#slname').val();
        var sdob = $('#sdob').val();
        $('#r_name').html(sfname+" "+slname);
        $('#r_dob').html(sdob);
        $('#r_relation').html(sdob);        

    }

</script>
<?php echo $__env->make('frontView.masterView', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>