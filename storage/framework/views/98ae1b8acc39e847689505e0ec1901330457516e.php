<?php $__env->startSection('main_body'); ?>

<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Wizard 6-->
                <div class="wizard wizard-6 d-flex flex-column flex-column-fluid" id="kt_wizard">
                    <!--begin::Container-->
                    <div class="wizard-content d-flex flex-column ml-10 mr-10 py-10 py-lg-20">
                        <!--begin::Nav-->
                        <div class="d-flex flex-column-auto flex-column px-10">
                            <!--begin: Wizard Nav-->
                            <div class="wizard-nav pb-lg-10 pb-3 d-flex flex-column align-items-center align-items-md-start">
                                <!--begin::Wizard Steps-->
                                <div class="wizard-steps d-flex flex-column flex-md-row">

                                </div>
                                <!--end::Wizard Steps-->
                            </div>
                            <!--end: Wizard Nav-->
                        </div>
                        <!--end::Nav-->

                        <!--begin::Form-->
                        <form method="post" action="updateProfile">
                            <?php echo e(csrf_field()); ?>

                            <!--begin: Wizard Step 1-->
                                <!--begin::Title-->
                                <div class="row">
                                    <div class="offset-3 col-md-12 col-sm-12">
                                        <h3 class="mb-10 font-weight-bold text-dark">Setting... </h3>
                                    </div>
                                    
                                    <div class="offset-3 col-md-6 col-sm-12">
                                        <?php if(session()->has('success')): ?>
                                        <div class="alert alert-success">
                                            <?php echo e(session()->get('success')); ?>

                                        </div>
                                        <?php endif; ?>
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>First Name</label>
                                            <input type="text" id="fname" name="fname" value="<?php echo e(Session()->get('user')->fname); ?>" placeholder="First Name" required class="form-control form-control-solid form-control-lg" >
                                        </div>
                                                                                
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>Last Name</label>
                                            <input type="text" name="lname" placeholder="Last Name" value="<?php echo e(Session()->get('user')->lname); ?>" required class="form-control form-control-solid form-control-lg" >
                                        </div>
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>Email</label>
                                            <input type="email" name="email" readonly placeholder="Email" value="<?php echo e(Session()->get('user')->email); ?>" required class="form-control form-control-solid form-control-lg" >
                                        </div>
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>Password</label>
                                            <input type="password" name="password" value="<?php echo e(Session()->get('user')->password); ?>" placeholder="Password" required class="form-control form-control-solid form-control-lg" >
                                        </div>
                                    </div>
                                 
                                    
                                </div>
                                <div class="offset-6">
                                    <button class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" type="submit" id="kt_login_signup_form_submit_button">Update 
                                    <span class="svg-icon svg-icon-md ml-2">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Navigation/Right-2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1"></rect>
                                                <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                            </g>
                                        </svg>
                                    </span></button>
                                </div>

                            
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Wizard 6-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>
    <!--end::Container-->
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontView.masterView', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>