

<?php $__env->startSection('main_body'); ?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Wizard-->
                <div class="wizard wizard-1" id="kt_wizard_v1" data-wizard-state="first" data-wizard-clickable="false">
                    <!--begin::Wizard Nav-->
                    <!--end::Wizard Nav-->
                    <!--begin::Wizard Body-->
                    <div class="row  my-10 px-8 my-lg-15 px-lg-10">
                        <div class="col-xl-12 col-xxl-7">
                            <!--begin::Wizard Form-->
                            <form method="post" action="/saveUser" class="form fv-plugins-bootstrap fv-plugins-framework" id="kt_form">
                                <?php echo e(csrf_field()); ?>

                                <div class="pb-5">
                                    <h3 class="font-weight-bold text-dark">Admin Users</h3>
                                    <span class="mb-10 form-text text-muted">Create a Admin user Profile from here, and give the user role from other section.</span>
                                    <!--begin::Input-->
                                    <div class="row">
                                        <div class="form-group col-md-6 fv-plugins-icon-container">
                                            <label>First Name</label>
                                            <input type="text" class="form-control form-control-solid form-control-lg" required name="fname" placeholder="First Name">
                                        </div>
                                        <div class="form-group col-md-6 fv-plugins-icon-container">
                                            <label>Last Name</label>
                                            <input type="text" required class="form-control form-control-solid form-control-lg" required name="lname" placeholder="Last Name">
                                        </div>
                                    </div>
                                    <div class="form-group fv-plugins-icon-container">
                                        <label>Role</label>
                                        <select name="role" required class="form-control form-control-solid form-control-lg">
                                            <option>Select Role</option>
                                            <option value="manager">Manager</option>
                                            <option value="user">User</option>
                                        </select>
                                    </div>
                                    <div class="form-group fv-plugins-icon-container">
                                        <label>Email</label>
                                        <input type="email" class="form-control form-control-solid form-control-lg" required name="email" placeholder="Email Address">
                                    </div>
                                    <div class="form-group fv-plugins-icon-container">
                                        <label>Create Password</label>
                                        <input type="password" class="form-control form-control-solid form-control-lg" required name="password" placeholder="Create Password">
                                    </div>
                                    <div class="form-group fv-plugins-icon-container">
                                        <label>Confirm Password</label>
                                        <input type="password" class="form-control form-control-solid form-control-lg" required name="confirmPassword" placeholder="Confirm Password">
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between mt-5 pt-10">
                                    <div class="mr-2">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-next">Save</button>
                                    </div>
                                </div>
                            </form>
                    </div>
                    <!--end::Wizard Body-->
                </div>

         

                <!--end::Wizard-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>




</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontView.masterView', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>