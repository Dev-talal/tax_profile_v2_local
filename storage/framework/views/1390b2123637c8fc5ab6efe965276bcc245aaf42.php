<?php $__env->startSection('main_body'); ?>

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <div class="d-flex justify-content-between pt-10 pb-10 pr-10">
                    <div class="mr-2">
                    </div>
                    <div>
                        <a href="<?php echo e(url('/addCountry')); ?>" class="btn btn-success font-weight-bold text-uppercase px-9 py-4">Add New</a>
                    </div>
                </div>

                <div class="example-preview">
                    <table class="table mb-5">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">First</th>
                                <th scope="col">Last</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Nick</td>
                                <td>Stone</td>
                                <td>
                                    <span class="label label-inline label-light-primary font-weight-bold">Pending</span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Ana</td>
                                <td>Jacobs</td>
                                <td>
                                    <span class="label label-inline label-light-success font-weight-bold">Approved</span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Larry</td>
                                <td>Pettis</td>
                                <td>
                                    <span class="label label-inline label-light-danger font-weight-bold">New</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
            

            </div>
            <!--end::Wizard-->
        </div>
    </div>


<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Wizard-->

                

                <!--end::Wizard-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>




</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontView.masterView', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>