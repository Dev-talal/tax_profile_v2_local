<html>
<head>
    <style>
        table {
          border-collapse: collapse;
          width: 100%;
        }
        
        th, td {
          padding: 8px;
          text-align: left;
          border-bottom: 1px solid #ddd;
        }
        </style>
            
            
</head>
<body>
    <div class="">
        <div class="row mt-5">
            <div class="col-md-6">
                <h3>Name: <?php echo e($client->fname.' '.$client->lname); ?></h3>
                <p>US Information Request</p> 
            </div>
            <div style="text-align: right" class="col-md-6">
            
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-9 col-sm-9">
                <h4><u>General Request</u></h4>
                
                <table style="width: 100%" class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Comment</th>
                        <th>Provided</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tbody>
                            <tr>
                                <td>1. Tax Organizar</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>2. FBAR Organizar</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>3. IRS Notice</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>4. Last us tax return file</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </tbody>
                </table>
            </div>
            <div class="col-md-3 col-sm-3">
                    <div class="vl"></div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <h4><u>Income Type</u></h4>
                <table style="width: 100%" class="table">
                    <thead>
                      <tr>
                        <th>Income Type</th>
                        <th>Country</th>
                        <th>Form Required</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tbody>
                            <?php $__currentLoopData = $txtProfileDocList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?= $tp->docx->name ?></td>
                                    <td><?= $tp->country_name->country_name ?></td>
                                    <td><?= $tp->dox ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
</body>
</html>