<?php $__env->startSection('main_body'); ?>


<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="d-flex justify-content-between pt-10 pb-10 pr-10">
        <div class="ml-5">
        <h3>Clients</h3>
        </div>
        <div>
            <?php if(json_decode(Session::get('user')->role_data)->addClient == 1){ ?>
                <a href="<?php echo e(url('/taxProfile')); ?>" class="btn btn-success font-weight-bold text-uppercase px-9 py-4">Add New</a>
            <?php } ?>
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        
        <div class="container">
            <div class="row">
                <?php $__currentLoopData = $clientList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-6 col-xl-4 mb-5">
                        <div class="card card-custom mb-8 mb-lg-0">
                            <div class="card-body">
                                <?php if(json_decode(Session::get('user')->role_data)->deleteClient == 1){ ?>
                                <p style="text-align: right"><a style="cursor:pointer" data-toggle="modal" data-target="#example<?= $client->id ?>">X</a></p>
                                <?php } ?>
                                <div class="d-flex align-items-center p-5">
                                    <div class="mr-6">
                                        <span class="svg-icon svg-icon-4x">
                                            <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Code/Compiling.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                                                </g>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <a href="/profileDetail/<?= $client->id ?>" class="text-dark text-hover-primary font-weight-bold font-size-h4 mb-3"><?= $client->fname.' '.$client->lname ?></a>
                                        <div class="text-dark-75">DOB: <?= $client->dob ?></div>
                                        <div class="text-dark-75">City: <?= $client->city ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Iconbox-->
                    </div>
                    <div class="modal fade" id="example<?= $client->id ?>" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Delete?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i aria-hidden="true" class="ki ki-close"></i>
                                    </button>
                                </div>
                                <div class="modal-body" >
                                    <h3>Are you Sure, You wants to delete?</h3>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">No</button>
                                    <a href="deleteProfile/<?= $client->id ?>" class="btn btn-primary font-weight-bold">Yes</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                
              
            </div>
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>





<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontView.masterView', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>