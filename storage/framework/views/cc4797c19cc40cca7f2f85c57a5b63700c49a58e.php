<?php $__env->startSection('main_body'); ?>


<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        
        <div class="container">
          
          <div class="card card-custom">
            <div class="card-body p-0">
                <h5 class="pt-5 ml-2">Clients</h5>
                <div class="d-flex justify-content-between pt-5 pb-2 pr-10">
                    <div class="mr-2">
                      <input class="ml-2 form-control" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search by names.." title="Type in a name">
                    </div>
                    <div>
                        <?php if(json_decode(Session::get('user')->role_data)->addAdmin == 1){ ?>
                            <a href="<?php echo e(url('/taxProfile')); ?>" class="btn btn-success btn-sm font-weight-bold text-uppercase px-9 py-4">Add New</a>
                        <?php } ?>
                    </div>
                </div>

                <div class="example-preview">
                    <table class="table mb-5" id="datatable">
                        <thead class="thead">
                            <tr>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">DOB</th>
                                <th scope="col">City</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                      <tbody>
                        <?php $__currentLoopData = $clientList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                              <td><?= $client->fname ?></td>
                              <td><?= $client->lname ?></td>
                              <td><?= $client->dob ?></td>
                              <td><?= $client->city ?></td>
                              <td>
                                 <a href="/profileDetail/<?= $client->id ?>" class="btn btn-icon btn-light btn-hover-primary btn-sm ">
                                    <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2020-08-03-114926/theme/html/demo1/dist/../src/media/svg/icons/General/Visible.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                <path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.3"/>
                                            </g>
                                        </svg>
                                     </span>
                                      </a>
                                 <a href="editProfile/<?= $client->id ?>" class="btn btn-icon btn-light btn-hover-primary btn-sm ">
                                    <span class="svg-icon svg-icon-md svg-icon-primary">
                                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                      <rect x="0" y="0" width="24" height="24"></rect>
                                                      <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953)"></path>
                                                      <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                  </g>
                                              </svg>
                                          </span>
                                      </a> 
                                        <?php if(json_decode(Session::get('user')->role_data)->deleteClient == 1){ ?>
                                                  <a style="cursor:pointer" data-toggle="modal" data-target="#example<?= $client->id ?>"  class="btn btn-icon btn-light btn-hover-primary btn-sm">
                                                      <span class="svg-icon svg-icon-md svg-icon-primary">
                                                          <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Trash.svg-->
                                                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                  <rect x="0" y="0" width="24" height="24"></rect>
                                                                  <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                  <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                                              </g>
                                                          </svg>
                                                          <!--end::Svg Icon-->
                                                      </span>
                                                  </a>
                                                <div class="modal fade" id="example<?= $client->id ?>" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Delete?</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                X
                                                            </button>
                                                        </div>
                                                        <div class="modal-body" >
                                                            <h3>Are you Sure, You wants to delete?</h3>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">No</button>
                                                            <a href="deleteProfile/<?= $client->id ?>" class="btn btn-primary font-weight-bold">Yes</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                          <?php } ?>
                              </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>

                    </table>
                    
                </div>
            

            </div>
            <!--end::Wizard-->
        </div>
          
            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>



<?php $__env->stopSection(); ?>


<script>
  window.addEventListener('load', function() {
    $('#datatable').DataTable();
    $('#datatable_filter').hide();
    $('#datatable_length').hide();
    });
  function myFunction() {
  var input, filter, table, tr, td, td1, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("datatable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    td1 = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent+" "+td1.textContent || td.innerText+" "+td1.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>



<?php echo $__env->make('frontView.masterView', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>