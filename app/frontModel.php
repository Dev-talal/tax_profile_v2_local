<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class frontModel extends Model
{
    protected  $table = 'countries';
    protected $fillable = [
        'country_name','created_at', 'status'
    ];
    public $timestamps = false;
}
