<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\formTypeModel;
use App\incomeTypeModel;
use App\RolesModel;
use App\IncomeTypeDataModel;
use App\UsersModel;
use DB;

class UpdateController extends Controller{
    
    public function updateFormType(Request $req)

    {

        formTypeModel::where('form_type_id',$req->form_id)->update(array(
        'form_type_name'=>$req->form_type));
        return redirect('/formType');
    }

    public function updateIncomeCategory(Request $req)

    {

        incomeTypeModel::where('income_type_id',$req->income_category_id)->update(array(
        'income_type_name'=>$req->income_category_name));
        return redirect('/incomeTypeCategory');
    }

    public function updateRoleType(Request $req)

    {

        RolesModel::where('id',$req->role_type_id)->update(array(
        'name'=>$req->role_type_name));
        return redirect('/roles');
    }

    public function updateAdminUser(Request $req)

    {

        UsersModel::where('id',$req->admin_id)->update(array(
        'fname'=>$req->admin_f_name,
        'lname'=>$req->admin_l_name,
        'role'=>$req->admin_role,
        'email'=>$req->admin_email,

    
    ));
        return redirect('/users');
    }

    public function updateIncomeType(Request $req)

    {

      

        IncomeTypeDataModel::where('id',$req->admin_id)->update(array(
        'name'=>$req->income_name,
        'income_type_id'=>$req->incomeTypeCategory,
        'form_type'=>$req->formType,
        'min_fee'=>$req->min_fee,
        'max_fee'=>$req->max_fee,

    
    ));
        return redirect('/incomeTypeView');
    }

}
