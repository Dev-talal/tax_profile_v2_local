<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Storage;

class LoginController extends Controller{
    public function login(){
        return view('frontView.loginView');
    }

    public function authantication(Request $req){
        $username = $req->username;
        $password = $req->password;
        
        $users = DB::table('users')
                    ->select('users.*', 'roles.role_data', 'roles.name as role_name')
                    ->join('roles', 'users.role', '=', 'roles.id')
                     ->where('email', '=', $username)
                     ->where('password', '=', $password)
                     ->first();
        if($users != null){
            // $req->session()->put('user', $users);
            Session::put('user', $users);
            return redirect('/');
        }else{
            return back()->with('error', 'Wrong Email or Password!');
        }
    }

    public function logout(Request $req){
        $req->session()->flush();
        return 
        redirect('/login');
    }

    public function setting(Request $req){
        return view('frontView.settingView');
    }

    public function updateProfile(Request $req){
        $fname = $req->fname;
        $lname = $req->lname;
        $password = $req->newPassword;
        $url = '';
        if($req->image != null){
          $extension = $req->image->extension();
          $img64 = $req->img64;
          $data = explode( ',', $img64);
          $data = base64_decode($data[1]);
          $image_name= time().'.'.$extension;
          $path = storage_path('app/'.$image_name);
          file_put_contents($path, $data);
          $url = $image_name;
        }

      $user = $req->session()->get('user', '');
      if(isset($password)){
        if($url != ''){
        DB::table('users')
          ->where('id', $user->id)
          ->update(['fname' => $fname, 'lname' => $lname, 'password' => $password, 'image'=>$url ]);          
        }else{
        DB::table('users')
          ->where('id', $user->id)
          ->update(['fname' => $fname, 'lname' => $lname, 'password' => $password ]);
        }
      }else{
        if($url != ''){
          DB::table('users')
            ->where('id', $user->id)
            ->update(['fname' => $fname, 'lname' => $lname, 'image'=>$url]);
        }else{
          DB::table('users')
            ->where('id', $user->id)
            ->update(['fname' => $fname, 'lname' => $lname]);
        }
      }
        $users = DB::table('users')
              ->select('users.*', 'roles.role_data', 'roles.name as role_name')
              ->join('roles', 'users.role', '=', 'roles.id')
               ->where('users.id', '=', $user->id)
               ->first();
        Session::put('user', $users);
        return back()->with('success', 'Profile Updates successfully!');
    }

}
