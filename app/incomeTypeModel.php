<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class incomeTypeModel extends Model
{
    protected  $table = 'income_type_category_name';
    protected $fillable = [
        'income_type_name','created_at'
    ];
    public $timestamps = false;
}
