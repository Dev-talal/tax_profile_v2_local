<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//=======Home
Route::get('/', 'FrontController@index')->middleware('usersession');
//=======Country
Route::get('country', 'FrontController@countries')->middleware('usersession');
Route::get('addCountry', 'FrontController@showaddCountries')->middleware('usersession');
Route::post('addCountry', 'FrontController@addCountries')->middleware('usersession');
Route::get('deleteCountry/{id}', 'FrontController@deleteCountry')->middleware('usersession');
Route::get('changeCountryStatus', 'FrontController@changeCountryStatus');



//=========IncomType
Route::get('incomeTypeCategory', 'incomeController@incomeTypeCategories')->middleware('usersession');
Route::get('addIncomeType', 'incomeController@showIncomeTypeForm')->middleware('usersession');
Route::post('addIncomeType', 'incomeController@addIncomeType')->middleware('usersession');
Route::get('deleteIncomeType/{id}', 'incomeController@deleteIncomeType')->middleware('usersession');
Route::get('addIncomTypeData', 'incomeController@addIncomeTypeData')->middleware('usersession');
Route::get('incomeType', 'incomeController@incomeType')->middleware('usersession');
Route::get('incomeTypeView', 'incomeController@incomeTypeDataView')->middleware('usersession');
Route::post('newIncomeTypeData', 'incomeController@addNewIncomeTypeData')->middleware('usersession');
Route::get('deleteIncomeData/{id}', 'incomeController@deleteIncomeData')->middleware('usersession');
Route::get('editIncome/{id}', 'incomeController@editIncomeData')->middleware('usersession');
Route::post('editIncomeDataSave', 'incomeController@editIncomeDataSave')->middleware('usersession');

//=======FormTypes
Route::get('formType', 'FrontController@formType')->middleware('usersession');
Route::get('addFormType', 'FrontController@showformTypePage')->middleware('usersession');
Route::post('addFormType', 'FrontController@addFormType')->middleware('usersession');
Route::get('deleteFormType/{id}', 'FrontController@deleteFormType')->middleware('usersession');

//======Users
Route::get('users', 'FrontController@adminUsers')->middleware('usersession');
Route::get('addUsers', 'FrontController@addAdminUsers')->middleware('usersession');
Route::post('saveUser', 'FrontController@saveUser')->middleware('usersession');
Route::get('deleteUser/{id}', 'FrontController@deleteAdminUsers')->middleware('usersession');

//======Roles
Route::get('roles', 'rolesController@userRoleView')->middleware('usersession');
Route::get('addUserRole', 'rolesController@addUserRole')->middleware('usersession');
Route::post('newRole', 'rolesController@addNewUserRole')->middleware('usersession');
Route::get('deleteRole/{id}', 'rolesController@deleteRole')->middleware('usersession');
Route::get('editRole/{id}', 'rolesController@editRole')->middleware('usersession');
Route::post('editRoleSave', 'rolesController@editRoleSave')->middleware('usersession');

//======Tax Profile
Route::get('taxProfile', 'TaxProfileController@addTaxProfile')->middleware('usersession');
Route::post('saveTaxProfile', 'TaxProfileController@saveTaxProfile')->middleware('usersession');
Route::get('client', 'TaxProfileController@clientView')->middleware('usersession');
Route::get('deleteProfile/{id}', 'TaxProfileController@deleteProfile')->middleware('usersession');
Route::get('profileDetail/{id}', 'TaxProfileController@profileDetail')->middleware('usersession');
Route::get('profilePdf/{id}', 'TaxProfileController@profilePdf')->middleware('usersession');
Route::get('editProfile/{id}', 'TaxProfileController@editProfile')->middleware('usersession');
Route::post('/updateTaxProfile', 'TaxProfileController@updateProfile')->middleware('usersession');



//====Login
Route::get('login', 'LoginController@login');
Route::post('auth', 'LoginController@authantication');
Route::get('logout', 'LoginController@logout');
Route::get('setting', 'LoginController@setting');
Route::post('updateProfile', 'LoginController@updateProfile');


//========Update==============////
//////////////////////////////////

Route::post('updateCountry', 'FrontController@updateCountry');
Route::post('updateFormType', 'UpdateController@updateFormType');
Route::post('updateIncomeCategory', 'UpdateController@updateIncomeCategory');
Route::post('updateRoleType', 'UpdateController@updateRoleType');
Route::post('updateAdminUser', 'UpdateController@updateAdminUser');
Route::post('updateIncomeType', 'UpdateController@updateIncomeType');



/////////////////////////////////
/////////////////////////////////


